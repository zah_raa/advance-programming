#ifndef CONE_H
#define CONE_H
#include <iostream>
#include <string>
#include <cmath>
#include "Shape.h"

class Cone : public Shape
{
public:
	Cone(int _x, int _y, int _z, int r , int h);
	int volume() const;
	void scale(int factor);
	std::string type() const ;
private:
	int radius;
	int height;
};

#endif