#ifndef SPHERE_H
#define SPHERE_H
#include <iostream>
#include <string>
#include "Shape.h"

class Sphere : public Shape
{
public:
	Sphere(int _x, int _y, int _z, int r);
	int volume() const;
	void scale(int factor);
	std::string type() const;
private:
	int radius;
};

#endif
