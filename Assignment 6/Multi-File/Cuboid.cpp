#include "Cuboid.h"
#define P 3.14

using namespace std;

Cuboid::Cuboid(int _x, int _y, int _z, int w, int h, int d)
	: Shape(_x, _y, _z)
{
	if(w <= 0 || h <= 0 || d <= 0)
		throw IllegalArgumentException();

	width = w;
	height = h;
	depth = d;
}

int Cuboid::volume() const
{
	return width*height*depth;
}

void Cuboid::scale(int factor)
{
	width *= factor;
	height *= factor;
	depth *= factor;
}

string Cuboid::type() const
{
	return "Cuboid";
}