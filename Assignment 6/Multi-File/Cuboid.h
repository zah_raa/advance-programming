#ifndef CUBOID_H
#define CUBOID_H
#include <iostream>
#include <string>
#include "Shape.h"

class Cuboid : public Shape
{
public:
	Cuboid(int _x, int _y, int _z, int w, int h, int d);
	int volume() const;
	void scale(int factor);
	std::string type() const ;
private:
	int width;
	int height;
	int depth;
};

#endif