#include "Sphere.h"
#define P 3.14

using namespace std;

Sphere::Sphere(int _x, int _y, int _z, int r)
	: Shape(_x, _y, _z)
{
	if(r <= 0)
		throw IllegalArgumentException();
	radius = r;
}

int Sphere::volume() const
{
	return 4*P*pow(radius, 3)/3 ;
}

void Sphere::scale(int factor)
{
	radius *= factor;
}

string Sphere::type() const
{
	return "Sphere";
}