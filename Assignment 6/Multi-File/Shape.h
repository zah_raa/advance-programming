#ifndef SHAPE_H_
#define SHAPE_H_
#include <iostream>
#include <cmath>
#include <string>
#include "Exception.h"

class Shape
{
public:
	Shape(int _x, int _y, int _z) : x(_x), y(_y), z(_z) {}
	void move(int dx, int dy, int dz);
	virtual void scale(int factor) = 0;
	virtual int volume() const = 0;
	virtual std::string type() const = 0;
	friend std::ostream& operator<<(std::ostream& out, const Shape* shape);
protected:
	int x;
	int y;
	int z;
};

#endif