#include <iostream>
#include <string>
#include "Cone.h"
#define P 3.14

using namespace std;

Cone::Cone(int _x, int _y, int _z, int r, int h)
	:Shape(_x, _y, _z)
{
	if(r <= 0 || h <= 0)
		throw IllegalArgumentException();

	radius = r;
	height = h;
}

int Cone::volume() const
{
	return P*pow(radius,2)*height/3 ;
}

void Cone::scale(int factor)
{
	radius *= factor;
	height *= factor;
}

string Cone::type() const
{
	return "Cone";
}