#include "FileSystemElements.h"

using namespace std;

void FileSystemElements::make_root()
{
	elements.push_back(new Directory(0,"root",0));
}

void FileSystemElements::add_directory(int _id, string _title, int _parent_id)
{
	int bad_parent_id = 1;
	try
	{
		elements.push_back(new Directory(_id, _title, _parent_id));
		for(int i = 0; i < elements.size()-1 ; i++)
		{
			if(_id == elements[i]->get_id()) throw IdAlreadyExists();
			else if(_title == elements[i]->get_type() && _parent_id == elements[i]->get_parent_id()) 
				throw BadTitle();
			else if(elements[i]->get_type() == "Directory")
			{
				if(elements[i]->get_id() == _parent_id)
					bad_parent_id = 0;
			}
		}
		if(bad_parent_id) throw BadParentId();
	}
	catch(IdAlreadyExists ia)
	{
		cerr << ia.what();
		elements.pop_back();	
	}
	catch(BadTitle bt)
	{
		cerr << bt.what();
		elements.pop_back();
	}
	catch(BadParentId bp)
	{
		cerr << bp.what();
		elements.pop_back();	
	}
}

void FileSystemElements::add_file(int _id, std::string _title, std::string _content, int _parent_id)
{
	int bad_parent_id = 1;
	try
	{
		elements.push_back(new File(_id, _title,_content, _parent_id));
		for(int i = 0; i < elements.size()-1 ; i++)
		{
			if(_id == elements[i]->get_id()) throw IdAlreadyExists();
			else if(_title == elements[i]->get_type() && _parent_id == elements[i]->get_parent_id()) 
				throw BadTitle();
			else if(elements[i]->get_type() == "Directory")
			{
				if(elements[i]->get_id() == _parent_id)
					bad_parent_id = 0;
			}
		}
		if(bad_parent_id) throw BadParentId();
	}
	catch(IdAlreadyExists ai)
	{
		cerr << ai.what();
		elements.pop_back();	
	}
	catch(BadTitle bt)
	{
		cerr << bt.what();
		elements.pop_back();	
	}
	catch(BadParentId bp)
	{
		cerr << bp.what();
		elements.pop_back();	
	}
}

void FileSystemElements::add_link(int _id, std::string _title, int _element_id, int _parent_id)
{
	int bad_parent_id = 1, bad_linked_id = 1 ;
	try
	{
		elements.push_back(new Link(_id, _title,_element_id, _parent_id));
		for(int i = 0; i < elements.size()-1 ; i++)
		{
			if(_id == elements[i]->get_id()) throw IdAlreadyExists();
			else if(_title == elements[i]->get_type() && _parent_id == elements[i]->get_parent_id()) 
				throw BadTitle();
			else if(elements[i]->get_type() == "Directory")
			{
				if(elements[i]->get_id() == _parent_id)
					bad_parent_id = 0;
			}
			if((elements[i]->get_type() == "File" || elements[i]->get_type() == "Directory") 
				&& elements[i]->get_id() == _element_id)
					bad_linked_id = 0;
		}
		if(bad_parent_id) throw BadParentId();
		else if(bad_linked_id) throw BadLinkedElement();
	}
	catch(IdAlreadyExists ia)
	{
		cerr << ia.what();
		elements.pop_back();	
	}
	catch(BadTitle bt)
	{
		cerr << bt.what();
		elements.pop_back();	
	}
	catch(BadParentId bp)
	{
		cerr << bp.what();
		elements.pop_back();	
	}
	catch(BadLinkedElement bl)
	{
		cerr << bl.what();
		elements.pop_back();	
	}
}

void FileSystemElements::find_id_error(int _id)
{
	int bad_element_id = 1;
	for(int i = 0; i < elements.size(); i++)
	{
		if(elements[i]->get_id() == _id)
			bad_element_id = 0;
	}
	if(bad_element_id)
		throw BadElementId();
}

void FileSystemElements::view_sub_directory(int _id, std::string _title)
{
	for(int j = 0; j < elements.size(); j++)
	{
		if(elements[j]->get_parent_id() == _id  && elements[j]->get_title() != _title)
		{
			cout << "Title: " << elements[j]->get_title();
			cout << ", Type: " << elements[j]->get_type() << endl;
		}
	}
}

void FileSystemElements::view_sub_link(int _element_id, std::string _title)
{
	for(int k = 0; k < elements.size() ; k++)
	{
		if(elements[k]->get_id() == _element_id)
		{
			if(elements[k]->get_type() == "File") elements[k]->view();
			else if(elements[k]->get_type() == "Directory")
			{
				elements[k]->view();
				view_sub_directory(_element_id, _title);
			}
		}
	}
}

void FileSystemElements::view(int _id)
{
	try
	{
		find_id_error(_id);
		for(int i = 0; i < elements.size(); i++)
		{
			if(elements[i]->get_id() == _id)
			{
				string type = elements[i]->get_type();
				string _title = elements[i]->get_title();
				if(type == "File") elements[i]->view();
				else if(type == "Directory")
				{
					elements[i]->view();
					view_sub_directory(_id, _title);
				}
				else if(type == "Link")
				{
					elements[i]->view();
					int _element_id = elements[i]->get_element_id();
					view_sub_link(_element_id, _title);
				}
			}
		}
	}
	catch(BadElementId be)
	{
		cerr << be.what();
	}
}

