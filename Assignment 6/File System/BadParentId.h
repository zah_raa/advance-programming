#ifndef BADPARENTID_H
#define BADPARENTID_H

#include <iostream>
#include <exception>
#define BADPARENTID "Parent ID is not referring to a directory!"

using namespace std;

class BadParentId : public exception
{
public:
	virtual const char* what() const throw() { return BADPARENTID; }
};

#endif

