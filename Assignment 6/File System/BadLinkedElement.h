#ifndef BADLINKEDELEMENT_H
#define BADLINKEDELEMENT_H

#include <iostream>
#include <exception>
#define BADLINKEDELEMENT "Invalid element type requested!"

using namespace std;

class BadLinkedElement : public exception
{
public:
	virtual const char* what() const throw() { return BADLINKEDELEMENT; }
};

#endif