#ifndef DIRECTORY_H
#define DIRECTORY_H

#include <string>
#include <iostream>
#include "Parent.h"

class Directory : public Parent
{
public:
	Directory(int _id, std::string _title, int p_id);
	std::string get_type() { return "Directory"; }
	int get_element_id() { return -1; }
	void view();
};

#endif