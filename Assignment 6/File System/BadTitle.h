#ifndef BADTITLE_H
#define BADTITLE_H

#include <iostream>
#include <exception>
#define BADTITLE "Parent directory has the same child!"

using namespace std;

class BadTitle : public exception
{
public:
	virtual const char* what() const throw() { return BADTITLE; }
};

#endif

