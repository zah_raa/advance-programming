#ifndef LINK_H
#define LINK_H

#include <string>
#include <iostream>
#include "Parent.h"

class Link : public Parent
{
public:
	Link(int _id, std::string _title, int el_id, int p_id);
	std::string get_type() { return "Link"; }
	int get_element_id() { return element_id; }
	void view();
private:
	int element_id;
};

#endif