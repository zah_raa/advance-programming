#ifndef BADELEMENTID_H
#define BADELEMENTID_H

#include <iostream>
#include <exception>
#define INVALIDELEMENTID "Invalid element ID requested!"

using namespace std;

class BadElementId : public exception
{
public:
	virtual const char* what() const throw() { return INVALIDELEMENTID; }
};

#endif 