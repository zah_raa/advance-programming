#ifndef FILESYSTEMELEMENS_H
#define FILESYSTEMELEMENS_H

#include <iostream>
#include <string>
#include <vector>
#include "Parent.h"
#include "Directory.h"
#include "File.h"
#include "Link.h"
#include "BadElementId.h"
#include "IdAlreadyExists.h"
#include "BadTitle.h"
#include "BadParentId.h"
#include "BadLinkedElement.h"

class FileSystemElements
{
public:
	FileSystemElements() {}
	void make_root();
	void find_id_error(int _id);
	void add_directory(int _id, std::string _title, int _parent_id);
	void add_file(int _id, std::string _title, std::string _content, int _parent_id);
	void add_link(int _id, std::string _title, int _element_id, int _parent_id);
	void view_sub_directory(int _id, std::string _title);
	void view_sub_link(int _element_id, std::string _title);
	void view(int _id);
private:
	std::vector <Parent*> elements;
};

#endif