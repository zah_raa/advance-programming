#ifndef PARENT_H
#define PARENT_H

#include <string>

class Parent
{
public:
	Parent(int _id, std::string _title, int p_id): id(_id),title(_title),parent_id(p_id) {}
	int get_id() { return id; }
	int get_parent_id() { return parent_id; }
	std::string get_title(){ return title; }
	virtual int get_element_id() = 0;
	virtual std::string get_type() = 0;
	virtual void view() = 0;
protected:
	int id;
	std::string title;
	int parent_id;
};

#endif
