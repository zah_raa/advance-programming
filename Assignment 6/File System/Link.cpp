#include "Link.h"

using namespace std;

Link::Link(int _id, std::string _title, int el_id, int p_id)
	:Parent(_id, _title, p_id)
{
	element_id = el_id;
}

void Link::view()
{
	cout << "Title: " << title << endl;
}