#ifndef IDALREADYEXIST_H
#define IDALREADYEXIST_H

#include <iostream>
#include <exception>
#define IDALREADYEXIST "Requested ID already exists!"

using namespace std;

class IdAlreadyExists : public exception
{
public:
	virtual const char* what() const throw() { return IDALREADYEXIST; }
};

#endif
