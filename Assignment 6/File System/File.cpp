#include "File.h"

using namespace std;

File::File(int _id, std::string _title, std::string _content, int p_id)
	:Parent(_id,_title,p_id)
{
	content = _content;
}

void File::view()
{
	cout << "Title: " << title << endl;
	cout << "Content:" << endl << content << endl;
}