#ifndef FILE_H
#define FILE_H

#include <string>
#include <iostream>
#include "Parent.h"

class File : public Parent
{
public:
	File(int _id, std::string _title, std::string _content, int p_id);
	std::string get_type() { return "File"; }
	int get_element_id() { return -1;}
	void view();
private:
	std::string content;
};

#endif