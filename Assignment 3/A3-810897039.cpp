#include <iostream>
#include <iomanip>
#include <vector>
#include <string>
#include <fstream>
#define NUMOFCOURSES 12
#define WEEKDAYS 7
#define HHMM 28
using namespace std;

const string days[WEEKDAYS] = {"## Saturday", "## Sunday", "## Monday", "## Tuesday", "## Wednesday", "## Thursday", "## Friday"};
const string hhmm[HHMM] = {"07:00","07:30","08:00","08:30","09:00","09:30","10:00","10:30","11:00","11:30","12:00","12:30","13:00","13:30","14:00","14:30","15:00","15:30","16:00","16:30","17:00","17:30","18:00","18:30","19:00","19:30","20:00","20:30"};
const string inputDay[7] = {"SAT", "SUN", "MON", "TUE", "WED", "THE", "FRI"};

vector <char> row(275,'\0');
vector <vector <char> > rows(3,row);

struct Schedule
{
	string code, group, name;
	vector <string> day;
	vector <string> time;
};

vector <Schedule> courseList;

void readFile (string address, vector <vector <string> >& courses)
{
	string courseLine;
	string courseCode;
	string courseName;
	ifstream File;
	File.open(address);
	getline(File,courseLine);
	for (int i=0 ; i < NUMOFCOURSES ; i++)
	{
		getline(File,courseLine);
		int camma = courseLine.find(',');
		courseCode = courseLine.substr(0,camma);
		courses[i].push_back(courseCode);
		courseName=courseLine.substr(camma+1);
		courses[i].push_back(courseName);
	}

	File.close();
}

void timeTable (int numOfDay)
{
	cout << days[numOfDay] << endl<<endl;
	cout << hhmm[0];
	for (int j=1 ; j<HHMM ; j++)
		cout << setw(10) << right << hhmm[j];
	cout << endl;
	for (int k=0 ; k < 55 ; k++)
		cout << "_____";
	cout << endl;
}

int checkSpace (string line)
{
	int space=0;
	for (int i=0; i<line.length() ; i++)
	{
		if (line[i] == ' ')
			space++ ;
	}
	return space;
}

string findName (vector <vector <string> > courses, int i)
{
		for (int j=0 ; j< NUMOFCOURSES ; j++)
			if (courseList[i].code == courses[j][0])
				return courses[j][1];
}

void splitData (string line,vector <vector <string> > courses, int i)
{
	int space;
	space = checkSpace (line);
	courseList[i].code = line.substr(0,7);
	courseList[i].group = line.substr(9,1);
	courseList[i].name = findName(courses, i) + " (" + courseList[i].group + ")";
	switch (space)
	{
		case 2:
			courseList[i].day.push_back(line.substr(11,3));
			courseList[i].time.push_back(line.substr(15,11));
			break;
		case 4:
			courseList[i].day.push_back(line.substr(11,3));
			courseList[i].time.push_back(line.substr(15,11));
			courseList[i].day.push_back(line.substr(27,3));
			courseList[i].time.push_back(line.substr(31,11));
			break;
		case 6:
			courseList[i].day.push_back(line.substr(11,3));
			courseList[i].time.push_back(line.substr(15,11));
			courseList[i].day.push_back(line.substr(27,3));
			courseList[i].time.push_back(line.substr(31,11));
			courseList[i].day.push_back(line.substr(43,3));
			courseList[i].time.push_back(line.substr(47,11));
			break;
	}
}

bool isEmpty (int start, int end, int numOfRow)
{
	for (int i = start ; i <= end ; i++)
	{
		if (rows[numOfRow][i] != '\0' && rows[numOfRow][i] != ' ')
			return false;
	}
	return true;
}

int findEmptyLine (int start, int end, int sizeOfRows)
{
	if (sizeOfRows <= 0)
		return 0;
	if (isEmpty (start, end, sizeOfRows) == false)
		return sizeOfRows+1;
	findEmptyLine (start, end, sizeOfRows-3);
}

void printBoard (int start, int end, int emptyLine, string _name)
{
	rows[emptyLine][start] = '+';
	rows[emptyLine][end] = '+';
	rows[emptyLine+1][start] = '|';
	rows[emptyLine+1][end] = '|';
	rows[emptyLine+2][start] = '+';
	rows[emptyLine+2][end] = '+';
	for (int i=start+1 ; i<end ; i++)
	{
		rows[emptyLine][i] = '-';
		rows[emptyLine+2][i] = '-';
	  rows[emptyLine+1][i] = ' ';
	}

	for (int j=0 ; j<start ; j++)
	{
		if (rows[emptyLine][j] == '\0')
			rows[emptyLine][j] = ' ';
		if (rows[emptyLine+1][j] == '\0')
			rows[emptyLine+1][j] = ' ';
		if (rows[emptyLine+2][j] == '\0')
			rows[emptyLine+2][j] = ' ';
	}
}

void printList (int start, int end, string _name)
{
	int _row = rows.size()-1;
	int emptyLine= findEmptyLine (start, end,rows.size()-1);
	if (emptyLine >= rows.size())
	{
		for (int i=0 ; i<3 ; i++)
			rows.push_back(row);
	}
		printBoard (start, end, emptyLine, _name);
}

bool isCourse(int numOfInputDay)
{
	for (int j=0; j<courseList.size() ; j++)
	{
		for (int k=0 ; k< courseList[j].day.size() ; k++)
		{
			if (courseList[j].day[k] == inputDay[numOfInputDay])
				return true;
		}
	}
	return false;
}

int setTime (string Hour)
{
	for (int i=0 ; i< HHMM ; i++)
		if (Hour == hhmm[i])
			return i;
}

void setDay (int numOfInputDay)
{
	if  (numOfInputDay >= 7)
		return;
	timeTable (numOfInputDay);
	for (int j=0; j<courseList.size() ; j++)
	{
		for (int k=0 ; k< courseList[j].day.size() ; k++)
		{
			if (courseList[j].day[k] == inputDay[numOfInputDay])
			{
				string _name = courseList[j].name;
				string fullHour = courseList[j].time[k];
				string startTime = fullHour.substr(0,5);
				string endTime = fullHour.substr(6,5);
				int start = 10*(setTime (startTime))+2;
				int end = 10*(setTime (endTime))+1;
				printList (start, end, _name);
			}
		}
	}
	if (isCourse (numOfInputDay))
	{
		for (int i=0;i<rows.size();i++)
		{
			for (int j=0;j<rows[i].size();j++)
			{
				if (rows[i][j] != '\0')
					cout<<rows[i][j];
			}
		cout<<endl;
		}
	}
	if (numOfInputDay != 6)
		cout << endl;
	rows.clear();
	rows.push_back(row);
	rows.push_back(row);
	rows.push_back(row);
	setDay(numOfInputDay+1);
}

int main(int argc, char* argv[])
{
	int k=0;
	string line;
	vector <vector <string> > courses (12);
	readFile (argv[1], courses);
	while (	getline (cin,line))
	{
		courseList.push_back(Schedule());
		splitData (line, courses, k);
		k++ ;
	}
	cout << "# " << argv[2] << endl << endl;
	setDay(0);
	return 0;
}
