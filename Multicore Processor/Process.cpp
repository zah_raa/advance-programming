#include "Process.h"
using namespace std;

void Process::add_process (string line)
{
	process_id.push_back(Proc());
	id = process_id.size();
	index = process_id.size()-1;
	int space_mark = line.find(' ');
	line = line.substr(space_mark+1);
	space_mark = line.find(' ');
	process_id[index].pid = id;
	process_id[index].num_of_thread = atoi(line.substr(0,space_mark).c_str());
	for (int i = 0 ; i < process_id[index].num_of_thread ; i++)
	{
		process_id[index].tid.push_back(i+1);
		line = line.substr(space_mark+1);
		space_mark = line.find(' ');
		process_id[index].time_slice.push_back(atoi(line.substr(0,space_mark).c_str()));
	}
	cout << "Process with pid = " << id << " added!" << endl;
}

Proc Process::get_process_id () {return process_id[process_id.size()-1];}
