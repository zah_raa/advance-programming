#ifndef PROCESS_H
#define PROCESS_H
#include <iostream>
#include <vector>
#include <string>
#include <cstdlib>

using namespace std;

struct Proc
{
	int pid;
	int num_of_thread;
	vector <int> tid;
	vector <int> time_slice;
};

class Process
{
public:
	void add_process (string line);
	Proc get_process_id ();
private:
	int id;
	int index;
	vector <Proc> process_id;
};

#endif
