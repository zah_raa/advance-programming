#include "Core.h"
using namespace std;

void Core::add_core ()
{
	id = core_id.size() + 1;
	core_id.push_back(Core_pro());
	int index = core_id.size()-1;
	core_id[index].cid = id;
	cout << "Core with core ID = " << id << " successfully added!" << endl;
}

int Core::find_core ()
{
	int smallest = 0;
	for (int i = 0; i < core_id.size(); i++)
	{
		if (core_id[i].pt.size() < core_id[smallest].pt.size())
			smallest = i;
	}
	return smallest;
}

void Core::add_thread_to_core (Process& p,string line)
{
	p.add_process(line);
	for (int j = 0; j < p.get_process_id().tid.size(); j++)
	{
		int i = find_core();
		core_id[i].pt.push_back(Pro_data());
		core_id[i].pt[core_id[i].pt.size()-1].pid = p.get_process_id().pid;
		core_id[i].pt[core_id[i].pt.size()-1].tid = p.get_process_id().tid[j];
		core_id[i].pt[core_id[i].pt.size()-1].time_slice = p.get_process_id().time_slice[j];
	}
}

void Core::show_cores_stat ()
{
	for (int i = 0; i < core_id.size(); i++)
	{
		if (core_id[i].pt.size() == 0)
			cout << "Core number : " << core_id[i].cid << endl;
		else
		{
			cout << "Core number : " << core_id[i].cid << endl;
			for (int j = 0; j < core_id[i].pt.size(); j++)
			{
				cout << "Process ID : " << core_id[i].pt[j].pid << " - " << "Thread ID : " << core_id[i].pt[j].tid << endl;
				cout << "Number of time slots : " << core_id[i].pt[j].time_slice << endl;
			}
		}
	}
}

void Core::run_cores()
{
	for (int i = 0; i < core_id.size(); i++)
	{
		if (core_id[i].pt.size() != 0)
		{
			cout << "Core number : " << core_id[i].cid << endl;
			cout << "Process ID : " << core_id[i].pt[0].pid << " - " << "Thread ID : " << core_id[i].pt[0].tid << endl;
			core_id[i].pt[0].time_slice = (core_id[i].pt[0].time_slice) - 1;
			if (core_id[i].pt[0].time_slice != 0)
			{
				core_id[i].pt.push_back(Pro_data());
				core_id[i].pt[core_id[i].pt.size()-1].pid = core_id[i].pt[0].pid;
				core_id[i].pt[core_id[i].pt.size()-1].tid = core_id[i].pt[0].tid;
				core_id[i].pt[core_id[i].pt.size()-1].time_slice = core_id[i].pt[0].time_slice;
				core_id[i].pt.erase(core_id[i].pt.begin());
			}
			else
				core_id[i].pt.erase(core_id[i].pt.begin());
		}
	}
}

void Core::finish_tasks ()
{
	int biggest = 0;
	for (int i = 0; i < core_id.size(); i++)
	{
		int sum = 0;
		for (int j = 0; j < core_id[i].pt.size(); j++)
		{
			sum += core_id[i].pt[j].time_slice;
		}
		if (sum > biggest)
			biggest = sum ;
	}
	for (int j = 0; j < biggest; j++)
	{
		cout << "Time Slice : " << j+1 << endl;
		for (int i = 0; i < core_id.size(); i++)
		{
			if (core_id[i].pt.size() != 0)
			{
				cout << "Core number : " << core_id[i].cid << endl;
				cout << "Process ID : " << core_id[i].pt[0].pid << " - " << "Thread ID : " << core_id[i].pt[0].tid << endl;
				core_id[i].pt[0].time_slice = (core_id[i].pt[0].time_slice) - 1;
				if (core_id[i].pt[0].time_slice != 0)
				{
					core_id[i].pt.push_back(Pro_data());
					core_id[i].pt[core_id[i].pt.size()-1].pid = core_id[i].pt[0].pid;
					core_id[i].pt[core_id[i].pt.size()-1].tid = core_id[i].pt[0].tid;
					core_id[i].pt[core_id[i].pt.size()-1].time_slice = core_id[i].pt[0].time_slice;
					core_id[i].pt.erase(core_id[i].pt.begin());
				}
				else
					core_id[i].pt.erase(core_id[i].pt.begin());
			}
		}
	}
}

vector <Core_pro> Core::get_core () {return core_id;}
