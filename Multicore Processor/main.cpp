#include "Process.h"
#include "Core.h"

using namespace std;

int main()
{
  Process p;
	Core c;
	string l;
	while (getline(cin,l))
	{
		if (l == "add_core")
			c.add_core();
		if (l.substr(0,11) == "add_process")
			c.add_thread_to_core(p,l);
		if (l == "show_cores_stat")
			c.show_cores_stat();
		if (l == "run_cores")
			c.run_cores();
		if (l == "finish_tasks")
			c.finish_tasks();
	}

  return 0;
}
