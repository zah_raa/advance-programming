#ifndef CORE_H
#define CORE_H
#include <iostream>
#include <vector>
#include <string>
#include "Process.h"

using namespace std;

class Process;

struct Pro_data
{
	int pid;
	int tid;
	int time_slice;
};

struct Core_pro
{
	int cid;
	vector <Pro_data> pt;
};

class Core
{
	public:
		void add_core ();
		int find_core ();
		void add_thread_to_core (Process& p,string line);
		void show_cores_stat ();
		void run_cores ();
		void finish_tasks ();
		vector <Core_pro> get_core ();
	private:
		int id;
		vector <Core_pro> core_id;
};

#endif
